# PDI - Color: Actividad Práctica

Para el desarrollo de esta actividad, se creo un script en python, este script levanta una interfaz gráfica para que el usuario pueda interactuar utilizando TKinter.

Para la conversión de la imagen de RGB a YIQ y viceversa se utilizaron las matrices vistas en clases, así como también se aplico todos los pasos explicados para realizar este proceso.

Las pruebas que se realizaron para verificar el funcionamiento del código son parecidas a las mostradas en la diapositiva en clase.

Entonces el programa desarrollado levanta la siguente ventana, donde el boton superior permite elegir una imagen del sistema de archivos

![Screenshot](./screenshots/main.png)
![Screenshot](./screenshots/image_load.png)

Los textfields de la parte inferior permiten el ingreso de los parámetros __a__ y __b__ que realizan la modificación de la luminosidad y saturación de la imagen.

Una vez agregado esos valores, se procede a dar click en el botton __"Process Image"__ que se encargará de procesar la imagen con los parámetros ingresados, gerando los siguientes resultados.

Con alta saturación
![Screenshot](./screenshots/b_high.png)

Con baja saturación
![Screenshot](./screenshots/b_low.png)

Con alta luminosidad
![Screenshot](./screenshots/a_high.png)

Con baja luminosidad
![Screenshot](./screenshots/a_low.png)

Combinando ambos valores con b = 0, es decir en blanco y negro y __a__ mayor a 1

![Screenshot](./screenshots/mixed_1.png)

Combinando ambos valores con __b__ mayor a 1, y __a__ menor que 1.
![Screenshot](./screenshots/mixed_2.png)