import tkinter as tk
import os
from os.path import exists
import numpy as np

from tkinter import filedialog
from PIL import Image, ImageTk

def is_number(x):
    try:
        float(x)
        return True
    except ValueError:
        return False


def process_image():
    if (not is_number(paramenter_a_entry.get())):
        message.set('Verify the parameter "a"')
        return
    if (not is_number(paramenter_a_entry.get())):
        message.set('Verify the parameter "b"')
        return
    a = float(paramenter_a_entry.get())
    b = float(paramenter_b_entry.get())

    # Convert image to array.
    rgb_image = np.asarray(image_resized, dtype='float32')
    # Normalize RGB values, for this divide each value for 
    rgb_image /= 255.0

    w = rgb_image.shape[0]
    h = rgb_image.shape[1]

    m_yiq = np.zeros((w,h,3))
    
    # Convert RGB to YIQ
    m_yiq[:,:,0] = 0.299 * rgb_image[:,:,0] + 0.587 * rgb_image[:,:,1] + 0.114 * rgb_image[:,:,2]
    m_yiq[:,:,1] = 0.595716 * rgb_image[:,:,0] + -0.274453 * rgb_image[:,:,1] + -0.321263 * rgb_image[:,:,2]
    m_yiq[:,:,2] = 0.211456 * rgb_image[:,:,0] + -0.522591 * rgb_image[:,:,1] + 0.311135 * rgb_image[:,:,2]

    # Multiply by the coefficients "a" and "b"
    m_yiq[:,:,0] *= a
    m_yiq[:,:,1] *= b
    m_yiq[:,:,2] *= b

    # Checks if the values are within the expected range, in case it does not assign the maximum or minimum value as appropriate
    m_yiq[:,:,0] = np.where(m_yiq[:,:,0] > 1, 1, m_yiq[:,:,0])
    m_yiq[:,:,1] = np.where(abs(m_yiq[:,:,1]) > 0.5957, np.sign(m_yiq[:,:,1]) * 0.5957, m_yiq[:,:,1])
    m_yiq[:,:,2] = np.where(abs(m_yiq[:,:,2]) > 0.5226, np.sign(m_yiq[:,:,2]) * 0.5226, m_yiq[:,:,2])

    # Convert YIQ to RGB
    rgb_image[:,:,0] = 1 * m_yiq[:,:,0] + 0.9663 * m_yiq[:,:,1] + 0.6210 * m_yiq[:,:,2]
    rgb_image[:,:,1] = 1 * m_yiq[:,:,0] + -0.2721 * m_yiq[:,:,1] + -0.6474 * m_yiq[:,:,2]
    rgb_image[:,:,2] = 1 * m_yiq[:,:,0] + -1.1070 * m_yiq[:,:,1] + 0.311135 * m_yiq[:,:,2]

    # Denormalize values
    rgb_image *= 255

    # Show image in windows
    new_image = ImageTk.PhotoImage(Image.fromarray(np.uint8(rgb_image)).convert('RGB'))
    processed_image_lbl.configure(image=new_image)
    processed_image_lbl.image = new_image
    
def open_image_file():
    filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Choose an image file", filetypes = (("jpg files","*.jpg"),("png files","*.png")))
    if filename == ():
        return
    image = Image.open(filename, 'r')

    # Resize the image if it is larger than the window.
    w,h = image.size
    if (w > 600):
        h = int(h * (600/w))
        w = 600
    if (h > 800):
        w = int(w * (800/h))
        h = 800
    global image_resized
    image_resized = image.resize((w,h))
    image_for_lbl = ImageTk.PhotoImage(image_resized)
    original_image_lbl.configure(image=image_for_lbl)
    original_image_lbl.image = image_for_lbl
    # Resize window.
    window.geometry(str(w*2) + 'x' + str(h + 90))

window = tk.Tk()
window.title("IPDI Chromatic Space")
window.geometry("1200x300")
window.columnconfigure(1, weight=1)
window.columnconfigure(2, weight=1)

tk.Button(text="Open image file", command=open_image_file).grid(row=0, column=0, columnspan=4, sticky=tk.E+tk.W)

original_image_lbl = tk.Label(window)
original_image_lbl.grid(row=1, column=0, columnspan=2)
processed_image_lbl = tk.Label(window)
processed_image_lbl.grid(row=1, column=2, columnspan=2)

tk.Label(window, text="Value for 'a':").grid(row=2, column=0)
tk.Label(window, text="Value for 'b':").grid(row=3, column=0)

paramenter_a_entry = tk.Entry(window)
paramenter_a_entry.grid(row=2, column=1, sticky=tk.E+tk.W)
paramenter_b_entry = tk.Entry(window)
paramenter_b_entry.grid(row=3, column=1, sticky=tk.E+tk.W)

tk.Button(text="Process Image", command=process_image).grid(row=2, column=2, rowspan=2, columnspan=2, sticky=tk.S+tk.N+tk.E+tk.W)

message = tk.StringVar()

tk.Label(window, textvariable=message).grid(row=4, column=0, columnspan=4, sticky=tk.E+tk.W)

window.mainloop()
