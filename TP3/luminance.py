from cgitb import text
import tkinter as tk
import os
import numpy as np
import matplotlib

from os.path import exists
from tkinter import ttk, filedialog
from PIL import Image, ImageTk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk
)
from matplotlib.ticker import PercentFormatter

matplotlib.use('TkAgg')

class Operation:
    def process(self, image, args=None):
        pass

class SqrtFilter(Operation):
    def process(self, image, args=None):
        result = np.copy(image)
        result[:,:,0] = np.sqrt(result[:,:,0])
        return result

class SquareFilter(Operation):
    def process(self, image, args=None):
        result = np.copy(image)
        result[:,:,0] = np.power(result[:,:,0], 2)
        return result

class LinealFilter(Operation):
    def process(self, image, args=None):
        min = 0.2
        max = 0.9
        result = np.copy(image)
        result[:,:,0] = np.where(result[:,:,0] <= min, 0, np.where(result[:,:,0] >= max, 1, result[:,:,0] * (1 / (max - min))))
        return result


# Convert RGB to YIQ
def rgb_to_yiq(rgb_image, original):
    h,w = original.size
    m_yiq = np.zeros((w,h,3))
    m_yiq[:,:,0] = 0.299 * rgb_image[:,:,0] + 0.587 * rgb_image[:,:,1] + 0.114 * rgb_image[:,:,2]
    m_yiq[:,:,1] = 0.595716 * rgb_image[:,:,0] + -0.274453 * rgb_image[:,:,1] + -0.321263 * rgb_image[:,:,2]
    m_yiq[:,:,2] = 0.211456 * rgb_image[:,:,0] + -0.522591 * rgb_image[:,:,1] + 0.311135 * rgb_image[:,:,2]
    return m_yiq

# Convert YIQ to RGB
def yiq_to_rgb(yiq_image, original):
    h,w = original.size
    m_rgb = np.zeros((w,h,3))
    m_rgb[:,:,0] = 1 * yiq_image[:,:,0] + 0.9663 * yiq_image[:,:,1] + 0.6210 * yiq_image[:,:,2]
    m_rgb[:,:,1] = 1 * yiq_image[:,:,0] + -0.2721 * yiq_image[:,:,1] + -0.6474 * yiq_image[:,:,2]
    m_rgb[:,:,2] = 1 * yiq_image[:,:,0] + -1.1070 * yiq_image[:,:,1] + 0.311135 * yiq_image[:,:,2]
    return m_rgb

def check_and_resize(image):
    w,h = image.size
    if w > 390:
        h = int(h * (390/w))
        w = 390
    if h > 400:
        w = int(w * (400/h))
        h = 400
    return image.resize((w, h))

def open_image(dest, size_lbl):
    filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Choose an image file", filetypes = (("jpg files","*.jpg"),("png files","*.png")))
    if filename == () or filename is None or filename == '':
        return
    image = Image.open(filename, 'r')

    w,h = image.size
    size_lbl.set(str(w) + 'x' + str(h))
    image_resized = check_and_resize(image)
    image_for_lbl = ImageTk.PhotoImage(image_resized)
    dest.configure(image=image_for_lbl)
    dest.image = image_for_lbl
    return image

def open_and_load_image():
    global img
    global img_yiq
    img = open_image(image_a_lbl, img_a_size_lbl)
    if img is not None:
        img_rgb = np.asarray(img, dtype='float') / 255.0
        img_yiq = rgb_to_yiq(img_rgb, img)
        w, h = img.size
        size = w*h
        data = img_yiq[:,:,0].reshape(size)
        sub = figure.add_subplot(211)
        figure.gca().yaxis.set_major_formatter(PercentFormatter(1))
        sub.hist(data, bins=10, weights=(np.ones(size) / size))
        sub.plot()
        figure_canvas.draw()

def process_image():
    image = img_yiq
    operation = functions[filter_selected.get()]
    yiq_image = operation.process(image)

    w, h = img.size
    size = w * h
    data = yiq_image[:,:,0].reshape(size)
    sub = figure.add_subplot(212)
    sub.clear()
    figure.gca().yaxis.set_major_formatter(PercentFormatter(1))
    sub.hist(data, bins=10, weights=(np.ones(size) / size))
    figure_canvas.draw()    

    new_img = yiq_to_rgb(yiq_image, img)
    new_img *= 255
    
    new_img = Image.fromarray(np.uint8(new_img)).convert('RGB')

    new_img = check_and_resize(new_img)
    rgb_image = ImageTk.PhotoImage(new_img)
    image_b_lbl.configure(image=rgb_image)
    image_b_lbl.image = rgb_image


def check_conditions(event=None):
    if img is not None and filter_selected.get() != '':
        process_btn['state'] = tk.ACTIVE
    else:
        process_btn['state'] = tk.DISABLED

filters = ['', 'Raiz', 'Cuadrado', 'Lineal a trozoz']
img = None
img_yiq = None
img_b = None
img_rgb_b = None
img_yiq_b = None

functions = {
    'Raiz': SqrtFilter(),
    'Cuadrado': SquareFilter(),
    'Lineal a trozoz': LinealFilter()
}

window = tk.Tk()
window.title('IDPI Luminance')
window.geometry('1300x520')

image_a_lbl = tk.Label(window)
image_a_lbl.place(x=10, y=10)
figure = Figure(figsize=(5,3.9), dpi=100)
figure_canvas = FigureCanvasTkAgg(figure, window)
toolbar = NavigationToolbar2Tk(figure_canvas, window, pack_toolbar=False)
toolbar.update()
figure_canvas.get_tk_widget().place(x=400, y=10)
image_b_lbl = tk.Label(window)
image_b_lbl.place(x=900, y=10)

img_a_size_lbl = tk.StringVar()
tk.Label(window, textvariable=img_a_size_lbl).place(x=180, y=400)
img_b_size_lbl = tk.StringVar()
tk.Label(window, textvariable=img_b_size_lbl).place(x=580, y=400)

tk.Button(text='Open Image 1', command=open_and_load_image).place(x=150, y=420)
process_btn = tk.Button(text='Process', state=tk.DISABLED, command=process_image)
process_btn.place(x=470, y=420)
tk.Button(text='Salir', command=exit).place(x=570, y=420)

filter_selected = tk.StringVar()
filter_cmb = ttk.Combobox(values=filters, textvariable=filter_selected, state='readonly')
filter_cmb.bind('<<ComboboxSelected>>', check_conditions)
filter_cmb.place(x=130, y=470)

tk.Label(window, text='Filter').place(x=170, y=490)

message = tk.StringVar()
tk.Label(window, textvariable=message).place(x=510, y=470)

window.mainloop()